﻿using Avalonia.Media.Imaging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MastoPhotoShareUI.Models
{
    public class Statistics
    {
        public string Id { get; set; }
        public Bitmap? Filename { get; set; }
        public DateTime Published {  get; set; }
        public long Boosts { get; set; }
        public long Likes { get; set; }
    }
}
