using Avalonia.Controls;
using CommunityToolkit.Mvvm.Input;
using MastoPhotoShare.Core;

namespace MastoPhotoShareUI.ViewModels;

public class SettingsViewModel : ViewModelBase
{
    private AppData _settings;
    
    public RelayCommand<Window> CancelCommand { get; private set; }
    public RelayCommand<Window> SaveCommand { get; private set; }

    public SettingsViewModel()
    {
        CancelCommand = new RelayCommand<Window>(Cancel);
        SaveCommand = new RelayCommand<Window>(Save);
    }
    
    private AppData Settings
    {
        get
        {
            if (_settings == null)
                Read();
            return _settings;
        }
        set
        {
            _settings = value;
            OnPropertyChanged(nameof(Settings));
        }
    }

    public bool DryMode
    {
        get { return Settings.DryMode; }
        set
        {
            Settings.DryMode = value;
            OnPropertyChanged(nameof(DryMode));
        }
    }

    public string Instance
    {
        get { return Settings.Instance; }
        set
        {
            Settings.Instance = value;
            OnPropertyChanged(nameof(Instance));
        }
    }
    
    public string Token
    {
        get { return Settings.Token; }
        set
        {
            Settings.Token = value;
            OnPropertyChanged(nameof(Token));
        }
    }
    
    public string HotFolder
    {
        get { return Settings.HotFolder; }
        set
        {
            Settings.HotFolder = value;
            OnPropertyChanged(nameof(HotFolder));
        }
    }
    
    public string DefaultTags
    {
        get { return Settings.DefaultTags; }
        set
        {
            Settings.DefaultTags = value;
            OnPropertyChanged(nameof(DefaultTags));
        }
    }

    private void Read()
    {
        _settings = SettingsManager.ReadSettings();
    }
    
    public void Cancel(Window? window)
    {
        window?.Close();
    }

    public void Save(Window? window)
    {
        SettingsManager.UpdateSettings(Settings);

        window?.Close();
    }
}