﻿using System.Diagnostics;
using System;
using System.IO;
using MastoPhotoShare.Core;
using System.Timers;
using System.Collections.ObjectModel;
using System.Linq;
using Avalonia.Threading;
using MastoPhotoShareUI.Models;
using Avalonia.Media.Imaging;
using Avalonia.Controls;
using CommunityToolkit.Mvvm.Input;
using MastoPhotoShareUI.Views;

namespace MastoPhotoShareUI.ViewModels;

public partial class MainViewModel : ViewModelBase
{
    private string _appSettingsFile = "appsettings.json";
    private AppData _appSettings;
    private string _statusMessages = "";
    private string _appStatus = "Offline";
    private ObservableCollection<Statistics> _archiveFiles = new ObservableCollection<Statistics>();
    private ObservableCollection<UploadFile> _uploadFiles = new ObservableCollection<UploadFile>();
    private StatisticsHandler _statistics;
    private MastodonPublisher _publisher;
    private DateTime? _lastRefresh;
    private UploadFile _selectedFile;
    private Timer _updateTimer;

    public RelayCommand<Window> CloseCommand { get; private set; }
    public RelayCommand OpenSettingsCommand { get; private set; }
    public RelayCommand OpenHotFolderCommand { get; private set; }

    public MainViewModel()
    {
        CloseCommand = new RelayCommand<Window>(Close);
        OpenSettingsCommand = new RelayCommand(OpenSettings);
        OpenHotFolderCommand = new RelayCommand(OpenHotFolder);
        
        Init();
    }
    
    public string StatusMessages
    {
        get { return _statusMessages; }
        set { 
            _statusMessages = value;
            OnPropertyChanged(nameof(StatusMessages));
        }
    }

    public DateTime? LastRefresh
    {
        get { return _lastRefresh; }
        set
        {
            _lastRefresh = value;
            OnPropertyChanged(nameof(LastRefresh));
        }
    }

    public bool DryMode
    {
        get { return _appSettings.DryMode; }
    }

    public string AppStatus
    {
        get { return _appStatus; }
        set
        {
            _appStatus = value;
            OnPropertyChanged(nameof(AppStatus));
        }
    }

    public ObservableCollection<Statistics> ArchiveFiles
    {
        get { return _archiveFiles; }
        set
        {
            _archiveFiles = value;
            OnPropertyChanged($"{nameof(ArchiveFiles)}");
        }
    }

    public ObservableCollection<UploadFile> UploadFiles
    {
        get { return _uploadFiles; }
        set
        {
            _uploadFiles = value;
            OnPropertyChanged($"{nameof(UploadFiles)}");
        }
    }

    private void Init()
    {
        Trace.WriteLine("Init");
        if (!File.Exists(_appSettingsFile))
        {
            WriteStatusMessage(String.Format("No configuration file '{0}' found.", _appSettingsFile));
        }

        _appSettings = SettingsManager.ReadSettings();
        SettingsManager.SettingsChanged += HandleChangedSettings;

        _updateTimer = new Timer();
        _updateTimer.Elapsed += UpdateStatistics;
        _updateTimer.Interval = 10000;

        ActivatePublisher();
    }

    private void HandleChangedSettings(object sender, EventArgs e)
    {
        DeactivatePublisher();
        Init();
        OnPropertyChanged(nameof(DryMode));
    }

    private void ActivatePublisher()
    {
        try
        {
            _publisher = new MastodonPublisher(_appSettings);
            _publisher.TraceEvent += Publisher_TraceEvent;
            _publisher.Uploading += _publisher_Uploading;
            _publisher.Uploaded += _publisher_Uploaded;
            _publisher.Failed += _publisher_Failed;
            _publisher.NewFile += _publisher_NewFile;
            _publisher.Configure();
            _publisher.ActivateImageFolder();

            AppStatus = "Online - Hot folder is active";

            _statistics = new StatisticsHandler();
            _updateTimer.Enabled = true;

        }
        catch (AppRegistrationException are)
        {
            WriteStatusMessage("Could not register application. SocialPhotoShare terminated.");
            AppStatus = "Error. Could not register Application.";
            Trace.TraceError(are.Message);
        }
        catch (LoadAppRegistrationException lare)
        {
            WriteStatusMessage("Could not load app registration.");
            AppStatus = "Error. Could not load app registration";
            Trace.TraceError(lare.Message);
        }
        catch (DirectoryNotFoundException dnfe)
        {
            WriteStatusMessage("Hot folder not configured");
            AppStatus = "Error. No hot folder configured.";
            Trace.TraceError(dnfe.Message);
        }
    }

    private void DeactivatePublisher()
    {
        if (_updateTimer != null)
            _updateTimer.Enabled = false;
        if (_publisher != null)
            _publisher.DeactivateImageFolder();
    }

    private void _publisher_Failed(UploadFile e)
    {
        UploadFiles.FirstOrDefault(u => u.FileName == e.FileName)!.State = e.State;
        OnPropertyChanged(nameof(UploadFiles));
    }

    private void _publisher_Uploaded(UploadFile e)
    {
        UploadFiles.FirstOrDefault(u => u.FileName == e.FileName)!.State = e.State;
        UpdateStatistics(null, null);
    }

    private void _publisher_Uploading(UploadFile e)
    {
        var uploadedFile = UploadFiles.FirstOrDefault(u => u.FileName == e.FileName);
        if (uploadedFile != null)
        {
            uploadedFile.State = e.State;
        }
    }

    private void _publisher_NewFile(UploadFile e)
    {
        Dispatcher.UIThread.Post(() =>
        {
            if (UploadFiles.FirstOrDefault(u => u.FileName == e.FileName) != null)
            {
                UploadFiles.FirstOrDefault(u => u.FileName == e.FileName)!.State = e.State;
            }
            else
            {
                UploadFiles.Add(e);
            }
        });
    }

    private void UpdateStatistics(object source, ElapsedEventArgs e)
    {
        Dispatcher.UIThread.Post(() =>
        {
            ArchiveFiles.Clear();
            foreach (var file in _publisher.ArchiveFiles)
            {
                var stat = new Statistics()
                {
                    Id = file.Id,
                    Filename = LoadCoverBitmap(Path.Combine(_appSettings.HotFolder, "archive", file.Filename)),
                    Published = file.Published,
                    Boosts = file.Boosts,
                    Likes = file.Likes
                };
                ArchiveFiles.Add(stat);
            }
            LastRefresh = DateTime.Now;
        });
    }

    public Bitmap LoadCoverBitmap(string filename)
    {
        try
        {
            using (var stream = File.OpenRead(filename))
            {
                return Bitmap.DecodeToWidth(stream, 400);
            }
        } 
        catch (FileNotFoundException ex)
        {
            WriteStatusMessage($"{ex.Message}");
        }
        return null;
    }

    private void WriteStatusMessage(string text)
    {
        StatusMessages += text;
        StatusMessages += Environment.NewLine;
    }

    private void Publisher_TraceEvent(string message)
    {
        WriteStatusMessage($"{message}");
    }

    private void OpenHotFolder()
    {
        if (OperatingSystem.IsWindows())
        {
            Process.Start("explorer.exe", _appSettings.HotFolder);
        }
        else if (OperatingSystem.IsLinux())
        {
            using Process openFileManager = new Process
            {
                StartInfo = new ProcessStartInfo
                {
                    FileName = "dbus-send",
                    Arguments = "--print-reply --dest=org.freedesktop.FileManager1 /org/freedesktop/FileManager1 org.freedesktop.FileManager1.ShowItems array:string:\"file://" + _appSettings.HotFolder + "\" string:\"\"",
                    UseShellExecute = true
                }
            };
            openFileManager.Start();
        }
    }

    private void Close(Window? window)
    {
        window?.Close();
    }

    private void OpenSettings()
    {
        // TODO: Clean up and work with View Model here
        SettingsWindow window = new SettingsWindow();
        window.Show();
    }
}
