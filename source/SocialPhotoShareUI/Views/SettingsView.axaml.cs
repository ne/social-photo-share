using Avalonia;
using Avalonia.Controls;
using Avalonia.Interactivity;
using Avalonia.Markup.Xaml;
using MastoPhotoShareUI.ViewModels;

namespace MastoPhotoShareUI.Views;

public partial class SettingsView : UserControl
{
    public SettingsView()
    {
        InitializeComponent();
    }

    protected override void OnLoaded(RoutedEventArgs e)
    {
        DataContext = new SettingsViewModel();
        base.OnLoaded(e);
    }
}