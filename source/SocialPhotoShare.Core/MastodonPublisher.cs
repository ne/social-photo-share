﻿using System.ComponentModel.Design.Serialization;
using System.Text.Json;
using System.Text;
using Mastonet;
using Mastonet.Entities;
using MetadataExtractor;
using MetadataExtractor.Formats.Iptc;
using System.Timers;
using System.Diagnostics;
using Timer = System.Timers.Timer;

namespace MastoPhotoShare.Core
{
    public delegate void TraceReceived(string message);

    public delegate void UploadHandler(UploadFile fileInfo);

    public class MastodonPublisher
    {
        private AppData _configuration;
        private string _appName = "SocialPhotoShare";
        private string _mastoAppRegistrationFile = "appRegistration.json";
        private MastodonClient _client;
        private Account _currentUser;
        private StatisticsHandler _statisticsHandler;
        private List<UploadEntry> _archiveFiles = new List<UploadEntry>();
        private Timer _hotFolderTimer;
        
        public List<UploadEntry> ArchiveFiles
        {
            get { return _archiveFiles; }
            set { _archiveFiles = value; }
        }

        public event TraceReceived TraceEvent;
        public event UploadHandler Uploading;
        public event UploadHandler Uploaded;
        public event UploadHandler Failed;
        public event UploadHandler NewFile;

        public MastodonPublisher(AppData appData)
        {
            _configuration = appData;
            _statisticsHandler = new StatisticsHandler();

            System.Timers.Timer aTimer = new System.Timers.Timer();
            aTimer.Elapsed += new ElapsedEventHandler(UpdateStatistics);
            aTimer.Interval = 60000; 
            aTimer.Enabled = true;
        }

        private void UpdateStatistics(object source, ElapsedEventArgs e)
        {
            _statisticsHandler.UpdateStatistics(_client);
            var data = _statisticsHandler.GetStatistics();
            this.ArchiveFiles = data;

            if (data != null)
            {
                TraceEvent?.Invoke("Statistics:");
                foreach(var line in data)
                {
                    TraceEvent?.Invoke(line.ToString());
                }
            }
        }

        public async void Configure()
        {
            string configFile = _mastoAppRegistrationFile;

            //var instance = _configuration.Instance;
            //var authClient = new AuthenticationClient(instance);
            //AppRegistration registration = null;

            //if (!File.Exists(configFile))
            //{

            //    registration = authClient.CreateApp(_appName, null, null, GranularScope.Read | GranularScope.Write | GranularScope.Follow).Result;

            //    if (registration != null)
            //    {
            //        var json = JsonSerializer.Serialize(registration);
            //        if (!String.IsNullOrEmpty(json))
            //        {
            //            File.WriteAllText(configFile, json);
            //        }
            //    }
            //    else
            //    {
            //        throw new AppRegistrationException();
            //    }
            //}

            //if (registration == null)
            //{
            //    var json = File.ReadAllText(configFile);
            //    registration = JsonSerializer.Deserialize<AppRegistration>(json);
            //    authClient.AppRegistration = registration;
            //}

            //if (registration == null)
            //{
            //    throw new LoadAppRegistrationException();
            //}

            try
            {
                _client = new MastodonClient(_configuration.Instance, _configuration.Token);
                _currentUser = await _client.GetCurrentUser();
                TraceEvent?.Invoke("Logged in user: " + _currentUser.AccountName);
                Trace.TraceInformation("Logged in user: " + _currentUser.AccountName);
            } 
            catch (JsonException je)
            {
                TraceEvent?.Invoke($"Server Error: {je.Message}");
                TraceEvent?.Invoke("Try to reconnect later again.");
                Trace.TraceError(je.Message);
            }
            catch (Exception ex)
            {
                TraceEvent?.Invoke($"Error: {ex.Message}");
                TraceEvent?.Invoke("Check configuration.");
                Trace.TraceError(ex.Message);
            }
        }

        public void ActivateImageFolder()
        {
            Trace.TraceInformation("Activating hot folder ...");

            if (!System.IO.Directory.Exists(_configuration.HotFolder))
                throw new DirectoryNotFoundException(_configuration.HotFolder);

            var archiveFolder = Path.Combine(_configuration.HotFolder, "archive");
            if (!System.IO.Directory.Exists(archiveFolder))
                System.IO.Directory.CreateDirectory(archiveFolder);
            
            _hotFolderTimer = new Timer();
            _hotFolderTimer.Elapsed += HandleNewFiles;
            _hotFolderTimer.Interval = 5000;
            _hotFolderTimer.Enabled = true;

            Trace.TraceInformation("Hot folder activated.");
        }

        public void DeactivateImageFolder()
        {
            if (_hotFolderTimer != null)
            {
                _hotFolderTimer.Stop();
                _hotFolderTimer.Dispose();
            }
        }

        private void HandleNewFiles(object source, ElapsedEventArgs e)
        {
            _hotFolderTimer.Enabled = false;
            
            var files = System.IO.Directory.GetFiles(_configuration.HotFolder, "*.jpg");

            foreach (var fullFile in files)
            {
                var uploadInfo = new UploadFile() { FileName = Path.GetFileName(fullFile), State = UploadState.New };
                NewFile?.Invoke(uploadInfo);
            }
            
            foreach (var fullFile in files)
            {
                var filename = Path.GetFileName(fullFile);
                
                TraceEvent?.Invoke("New file added: " + filename);
                Trace.TraceInformation("New file added: " + filename);

                var uploadInfo = new UploadFile() { FileName = Path.GetFileName(fullFile), State = UploadState.Uploading };
                try
                {
                    if (!fullFile.Contains("archive"))
                    {
                        uploadInfo.State = UploadState.Uploading;
                        Uploading?.Invoke(uploadInfo);
                        HandleFile(fullFile);
                        
                        uploadInfo.State = UploadState.Uploaded;
                        Uploaded?.Invoke(uploadInfo);
                    }
                }
                catch (NoIPTCDataException nide)
                {
                    uploadInfo.State = UploadState.Error;
                    uploadInfo.ErrorMessage = "No IPTC data";
                    Failed?.Invoke(uploadInfo);
                }
                catch (Exception ex)
                {
                    Trace.TraceError(ex.Message);

                    uploadInfo.State = UploadState.Error;
                    uploadInfo.ErrorMessage = ex.Message;
                    
                    Failed?.Invoke(uploadInfo);
                }
            }

            _hotFolderTimer.Enabled = true;
        }
        
        private void HandleFile(string fullPath)
        {
            Thread.Sleep(2000);

            var types = ImageMetadataReader.ReadMetadata(fullPath);
            var iptc = types.OfType<IptcDirectory>().FirstOrDefault();
            if (iptc == null)
            {
                TraceEvent?.Invoke("No IPTC data for " + fullPath);
                throw new NoIPTCDataException();
            }
            
            if (!_configuration.DryMode)
            {
                var title = iptc.GetDescription(IptcDirectory.TagHeadline);
                var description = iptc.GetDescription(IptcDirectory.TagCaption);
                var keywords = iptc.GetDescription(IptcDirectory.TagKeywords);

                TraceEvent?.Invoke("Title:       " + title);
                TraceEvent?.Invoke("Description: " + description);
                TraceEvent?.Invoke("Keywords:    " + keywords);

                var media = new MediaDefinition(new FileStream(fullPath, FileMode.Open), Path.GetFileName(fullPath));

                var attachment = _client.UploadMedia(media).Result;

                attachment = _client.UpdateMedia(attachment.Id, description).Result;

                StringBuilder status = new StringBuilder();
                status.AppendLine(title);
                status.AppendLine();
                if (!String.IsNullOrEmpty(keywords))
                {
                    var defaultHashtags = _configuration.DefaultTags.Split(';', StringSplitOptions.RemoveEmptyEntries).ToList();
                    var hashtags = keywords.Split(';', StringSplitOptions.RemoveEmptyEntries).ToList();
                    hashtags.AddRange(defaultHashtags);

                    foreach (var hashtag in hashtags)
                    {
                        status.Append("#" + hashtag);
                        status.Append(" ");
                    }
                }
                
                var publishedStatus = _client.PublishStatus(status.ToString(), Visibility.Public, null, new string[] { attachment.Id }).Result;
                if (publishedStatus != null && !String.IsNullOrEmpty(publishedStatus.Id))
                {
                    TraceEvent?.Invoke("Published " + fullPath);
                    _statisticsHandler.AddUpload(new UploadEntry() { Id = publishedStatus.Id, Filename = Path.GetFileName(fullPath), Published = publishedStatus.CreatedAt });

                    File.Move(fullPath, Path.Combine(_configuration.HotFolder, "archive", Path.GetFileName(fullPath)));
                }
            } else
            {
                Trace.TraceInformation("NO UPLOAD: DRY MODE ON");
            }
        }
    }
}
