﻿
namespace MastoPhotoShare.Core
{
    public class UploadEntry
    {
        public string Id { get; set; }
        public DateTime Published { get; set; }
        public string Filename { get; set; }
        public string Title { get; set; }
        public long Boosts { get; set; }
        public long Likes { get; set; }

        public override string ToString()
        {
            return this.Id + " - " + this.Filename + " - " + this.Title + " - Likes: " + this.Likes + " - Boosts: " + this.Boosts;
        }
    }
}
