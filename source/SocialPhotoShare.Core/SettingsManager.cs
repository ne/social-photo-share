using System.Text.Json;

namespace MastoPhotoShare.Core;

public static class SettingsManager
{
    private static string _appSettingsFile = "appsettings.json";

    public static event EventHandler SettingsChanged;

    public static AppData ReadSettings()
    {
        if (!File.Exists(_appSettingsFile))
        {
            var newSettings = new AppData();
            UpdateSettings(newSettings);
            return newSettings;
        }

        var settings = File.ReadAllText(_appSettingsFile);
        var appSettings = JsonSerializer.Deserialize<AppData>(settings);
        return appSettings;
    }

    public static void UpdateSettings(AppData settings)
    {
        var json = JsonSerializer.Serialize(settings);
        File.WriteAllText(_appSettingsFile, json);
        SettingsChanged?.Invoke(null, new EventArgs());
    }
}