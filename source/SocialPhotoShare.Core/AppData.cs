﻿
namespace MastoPhotoShare.Core
{
    public class AppData
    {
        public bool DryMode { get; set; }
        public string Instance { get; set; }
        public string Token { get; set; }
        public string HotFolder { get; set; }
        public string DefaultTags { get; set; }
    }
}
