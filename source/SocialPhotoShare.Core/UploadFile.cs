using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace MastoPhotoShare.Core;

public enum UploadState
{
    New = 0,
    Uploading = 1,
    Uploaded = 2,
    Error = 9
}
public class UploadFile : INotifyPropertyChanged
{
    private string _filename;
    private UploadState _state = UploadState.New;
    private string _errorMessage;

    public string FileName
    {
        get { return _filename; }
        set
        {
            _filename = value;
            OnPropertyChanged(nameof(FileName));
        }
    }

    public UploadState State
    {
        get { return _state; }
        set
        {
            _state = value;
            OnPropertyChanged(nameof(State));
        }
    }

    public string ErrorMessage
    {
        get { return _errorMessage; }
        set
        {
            _errorMessage = value;
            OnPropertyChanged(nameof(ErrorMessage));
        }
    }

    public event PropertyChangedEventHandler? PropertyChanged;

    protected virtual void OnPropertyChanged([CallerMemberName] string? propertyName = null)
    {
        PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
    }

    protected bool SetField<T>(ref T field, T value, [CallerMemberName] string? propertyName = null)
    {
        if (EqualityComparer<T>.Default.Equals(field, value)) return false;
        field = value;
        OnPropertyChanged(propertyName);
        return true;
    }
}