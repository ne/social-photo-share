﻿using Mastonet;
using System.Text.Json;

namespace MastoPhotoShare.Core
{
    public class StatisticsHandler
    {
        private string _filename = "uploadinfos.json";
        private UploadData data = new UploadData();

        public StatisticsHandler()
        {
            Init();
        }

        private void Init()
        {
            if (File.Exists(this._filename))
            {
                var json = File.ReadAllText(this._filename);
                data = JsonSerializer.Deserialize<UploadData>(json);
            }           
        }

        private void PersistData()
        {
            var json = JsonSerializer.Serialize<UploadData>(data);
            File.WriteAllText(this._filename, json);
        }

        public void AddUpload(UploadEntry entry) 
        { 
            data.UploadEntries.Add(entry);
            PersistData();
        }

        public void UpdateStatistics(MastodonClient client)
        {
            foreach(var entry in data.UploadEntries)
            {
                var status = client.GetStatus(entry.Id).Result;
                entry.Boosts = status.ReblogCount;
                entry.Likes = status.FavouritesCount;
            }
            PersistData();
        }

        public List<UploadEntry> GetStatistics()
        {
            return data.UploadEntries;
        }
    }
}
