﻿namespace MastoPhotoShare.Core
{
    public class UploadData
    {
        public List<UploadEntry> UploadEntries { get; set; } = new List<UploadEntry>();
    }
}
