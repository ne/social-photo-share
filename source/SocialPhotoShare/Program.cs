﻿using MastoPhotoShare.Core;
using System.Diagnostics;
using System.Reflection;
using System.Text.Json;

internal class Program
{
    private static void Main(string[] args)
    {
        Console.WriteLine("SocialPhotoShare v" + Assembly.GetExecutingAssembly().GetName().Version.ToString());
        var settingsManager = new SettingsManager();

        AppData appSettings = settingsManager.ReadSettings();
        if (String.IsNullOrEmpty(appSettings.Instance))
        {
            Console.WriteLine("No instance configured");
            Trace.TraceError("No instance configured");
            Environment.Exit(1);
        }

        if (String.IsNullOrEmpty(appSettings.HotFolder))
        {
            Console.WriteLine("No hot folder configured");
            Trace.TraceError("No hot folder configured");
            Environment.Exit(1);
        }
        
        try
        {
            MastodonPublisher publisher = new MastodonPublisher(appSettings);
            publisher.TraceEvent += Publisher_TraceEvent;
            publisher.Configure();
            publisher.ActivateImageFolder();
        } 
        catch (AppRegistrationException are)
        {
            Console.WriteLine("Could not register application. SocialPhotoShare terminated.");
            Trace.TraceError(are.Message);
            Environment.Exit(1);
        }
        catch (LoadAppRegistrationException lare)
        {
            Console.WriteLine("Could not load app registration.");
            Trace.TraceError(lare.Message);
            Environment.Exit(2);
        }

        Console.ReadLine();
    }

    private static void Publisher_TraceEvent(string message)
    {
        Console.WriteLine($"{message}");
    }
}